<?php declare(strict_types=1);

namespace kor3k\Pagination\Adapter;

use kor3k\Pagination\Helper;
use Pagerfanta\PagerfantaInterface;

class PagerfantaAdapter implements AdapterInterface
{
    private PagerfantaInterface $pagerfanta;

    public function __construct(PagerfantaInterface $pagerfanta)
    {
        $this->pagerfanta = $pagerfanta;
    }

    public function getOffset(): int
    {
        return Helper::getOffsetForPage($this->pagerfanta->getCurrentPage(), $this->getLimit());
    }

    public function setOffset(int $offset)
    {
        $this->pagerfanta->setCurrentPage(Helper::getPageForOffset($offset, $this->getLimit()));
        return $this;
    }

    public function getLimit(): int
    {
        return $this->pagerfanta->getMaxPerPage();
    }

    public function setLimit(int $limit)
    {
        $this->pagerfanta->setMaxPerPage($limit);
        return $this;
    }

    public function getIterator()
    {
        return $this->pagerfanta->getIterator();
    }

    public function count()
    {
        return $this->pagerfanta->count();
    }
}
