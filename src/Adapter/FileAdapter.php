<?php

declare(strict_types=1);

namespace kor3k\Pagination\Adapter;

class FileAdapter implements AdapterInterface
{
    use OffsetLimitTrait;

    protected \SplFileObject $file;
    protected int $lines;

    public function __construct(\SplFileInfo $file)
    {
        if ($file instanceof \SplFileInfo) {
            $file = $file->openFile('r');
        } elseif (!$file instanceof \SplFileObject) {
            $file = new \SplFileObject($file, 'r');
        }

        //$file->setMaxLineLen(4096);
        $this->file = $file;
        $this->lines = $this->countLines($file);

        $this->setLimit($this->count()-1);
    }

    private function countLines(\SplFileObject $file): int
    {
        $file->seek(PHP_INT_MAX);
        $lines = (int) ($file->key() + 1);
        $file->rewind();
        
        return $lines;
    }

    public function getIterator()
    {
        $generator = function () {
            $this->file->seek($this->getOffset());

            for ($i = 0; $i < $this->getLimit(); $i++ , $this->file->next()) {
                if ($this->file->eof()) {
                    return;
                }

                yield $this->file->current();
            }
        };

        return new \NoRewindIterator($generator());
    }

    public function count()
    {
        return $this->lines;
    }
}
