<?php

declare(strict_types=1);

namespace kor3k\Pagination\Paginator;

use kor3k\Pagination\Adapter\AdapterInterface;

class Paginator implements PaginatorInterface
{
    protected AdapterInterface $adapter;

    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getItems(int $limit = null, int $offset = null): \Traversable
    {
        if (!is_null($limit)) {
            $this->setLimit($limit);
        }
        if (!is_null($offset)) {
            $this->setOffset($offset);
        }

        return $this->getIterator();
    }

    public function toArray(): array
    {
        return iterator_to_array($this->getIterator(), false);
    }

    public function getCurrentPage(): int
    {
        return $this->getPageForOffset($this->getOffset());
    }

    public function setCurrentPage(int $page): self
    {
        $this->setOffset($this->getOffsetForPage($page));

        return $this;
    }

    public function getPages(): int
    {
        $pages = (int)ceil($this->count() / $this->getLimit());

        return (int)$pages;
    }

    /**
     * @inheritdoc
     */
    public function getPagesOffset(): array
    {
        $cnt = $this->getPages();
        $pages = [];

        for ($i = 1; $i <= $cnt; ++$i) {
            $pages[$i] = $this->getOffsetForPage($i);
        }

        return $pages;
    }

    public function getOffsetForPage(int $page): int
    {
        $this->sanitizePage($page);

        $tmp = $page - 1;
        $offset = $tmp * $this->getLimit();

        return (int)$offset;
    }

    public function getPageForOffset(int $offset): int
    {
        $tmp = (int)floor($offset / $this->getLimit());
        $page = $tmp + 1;

        $this->sanitizePage($page);

        return (int)$page;
    }

    public function getItemsPerPage(): int
    {
        return $this->getLimit();
    }

    protected function sanitizePage(int &$page)
    {
        if ($page >= $this->getPages()) {
            $page = $this->getPages();
        }
        if ($page < 1) {
            $page = 1;
        }
    }

    public function setOffset(int $offset): self
    {
        $this->adapter->setOffset($offset);
        return $this;
    }

    public function getOffset(): int
    {
        return $this->adapter->getOffset();
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->adapter->getLimit();
    }

    public function setLimit(int $limit): self
    {
        $this->adapter->setLimit($limit);
        return $this;
    }

    public function count()
    {
        return $this->adapter->count();
    }

    public function getIterator()
    {
        return $this->adapter->getIterator();
    }
}
