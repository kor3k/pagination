<?php

declare(strict_types=1);

namespace kor3k\Pagination\Adapter;

class ArrayAdapter implements AdapterInterface
{
    use OffsetLimitTrait;

    protected array $items;

    public function __construct(array $items)
    {
        $this->items = $items;
        $this->setLimit($this->count()-1);
    }

    public function getIterator()
    {
        return new \ArrayIterator(
            \array_slice($this->items, $this->getOffset(), $this->getLimit(), true)
        );
    }

    public function count()
    {
        return \count($this->items);
    }
}
