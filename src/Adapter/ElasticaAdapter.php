<?php

declare(strict_types=1);

namespace kor3k\Pagination\Adapter;

use FOS\ElasticaBundle\Paginator\PaginatorAdapterInterface;

class ElasticaAdapter implements AdapterInterface
{
    use OffsetLimitTrait;

    protected PaginatorAdapterInterface $elasticaPaginator;

    public function __construct(PaginatorAdapterInterface $elasticaPaginator)
    {
        $this->elasticaPaginator = $elasticaPaginator;
    }

    public function getIterator()
    {
        return new \ArrayIterator(
            $this->elasticaPaginator->getResults($this->getOffset(), $this->getLimit())->toArray()
        );
    }

    public function count()
    {
        return $this->elasticaPaginator->getTotalHits();
    }
}
