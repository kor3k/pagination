<?php

declare(strict_types=1);

namespace kor3k\Pagination\Paginator;

interface PaginatorInterface extends \Countable, \IteratorAggregate
{
    public function setOffset(int $offset);
    public function getOffset(): int;
    public function getLimit(): int;
    public function setLimit(int $limit);
    public function getPages(): int;
    public function getCurrentPage(): int;
    public function setCurrentPage(int $page);
    public function toArray(): array;

    public function getItems(int $limit = null, int $offset = null): \Traversable;

    /**
     * @return array page# => offset
     */
    public function getPagesOffset(): array;
    public function getOffsetForPage(int $page): int;
    public function getPageForOffset(int $offset): int;
    public function getItemsPerPage(): int;
}
