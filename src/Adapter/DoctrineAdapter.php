<?php

declare(strict_types=1);

namespace kor3k\Pagination\Adapter;

use Doctrine\ORM\Tools\Pagination\Paginator as BasePaginator;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

class DoctrineAdapter extends BasePaginator implements AdapterInterface
{
    /**
     * @param Query|QueryBuilder $query
     * @param bool $fetchJoinCollection
     */
    public function __construct($query, $fetchJoinCollection = true)
    {
        if (!($query instanceof Query || $query instanceof QueryBuilder)) {
            throw new \UnexpectedValueException(sprintf('$query must be of type %s | %s', Query::class, QueryBuilder::class));
        }

        parent::__construct($query, $fetchJoinCollection);
    }

    public function getOffset(): int
    {
        return $this->getQuery()->getFirstResult();
    }

    public function setOffset(int $offset): self
    {
        $this->getQuery()->setFirstResult($offset);
        return $this;
    }

    public function getLimit(): int
    {
        return $this->getQuery()->getMaxResults();
    }

    public function setLimit(int $limit): self
    {
        $this->getQuery()->setMaxResults($limit);
        return $this;
    }
}
