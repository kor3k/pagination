<?php

declare(strict_types=1);

namespace kor3k\Pagination\Adapter;

trait OffsetLimitTrait
{
    protected int $offset = 0;
    protected int $limit = 20;

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;
        return $this;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }
}
