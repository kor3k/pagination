<?php

declare(strict_types=1);

namespace kor3k\Pagination;

final class Helper
{
    public static function getPageForOffset(int $offset, int $limit): int
    {
        $tmp = (int)floor($offset / $limit);
        $page = $tmp + 1;

        if ($page < 1) {
            $page = 1;
        }

        return (int)$page;
    }

    public static function getOffsetForPage(int $page, int $limit): int
    {
        if ($page < 1) {
            $page = 1;
        }

        $tmp = $page - 1;
        $offset = $tmp * $limit;

        return (int)$offset;
    }
}
