<?php

declare(strict_types=1);

namespace kor3k\Pagination\Adapter;

class AggregatedAdapter implements AdapterInterface
{
    use OffsetLimitTrait;

    private array $adapters;

    public function __construct(AdapterInterface ...$adapters)
    {
        $this->adapters = $adapters;
    }

    public function getIterator()
    {
        $iterator = new \AppendIterator();

        foreach ($this->adapters as $adapter) {
            $iterator->append($adapter->getIterator());
        }

        return new \LimitIterator($iterator, $this->getOffset(), $this->getLimit());
    }

    public function count()
    {
        $total = 0;

        foreach ($this->adapters as $adapter) {
            $total += $adapter->count();
        }

        return $total;
    }
}
