<?php

declare(strict_types=1);

namespace kor3k\Pagination\Adapter;

class RangeAdapter implements AdapterInterface
{
    use OffsetLimitTrait;

    protected int $count;

    public function __construct(int $count)
    {
        $this->count = $count;
        $this->setLimit($this->count()-1);
    }

    public function getIterator()
    {
        $offset = $this->getOffset();
        $offset = $offset ?: 1;
        $limit = $offset + $this->getLimit();
        $limit = $limit > $this->count() ? $this->count() : $limit - 1;

        return new \ArrayIterator(
            range($offset, $limit)
        );
    }

    public function count()
    {
        return $this->count;
    }
}
