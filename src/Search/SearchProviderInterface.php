<?php

declare(strict_types=1);

namespace kor3k\Pagination\Search;

use kor3k\Pagination\Paginator\PaginatorInterface;

interface SearchProviderInterface
{
    public function search(string $query, int $offset, int $limit): PaginatorInterface;
}
