<?php

declare(strict_types=1);

namespace kor3k\Pagination\Adapter;

use DH\Auditor\Provider\Doctrine\Persistence\Reader\Query;
use kor3k\Pagination\Helper;

class AuditAdapter implements AdapterInterface
{
    use OffsetLimitTrait;

    private Query $query;

    public function __construct(Query $query)
    {
        $this->query = $query;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->query->limit($this->getLimit(), $this->getOffset())->execute());
    }

    public function count()
    {
        return $this->query->count();
    }
}
