<?php

declare(strict_types=1);

namespace kor3k\Pagination\Paginator;

class ExtractedPaginator extends Paginator
{
    private array $items;
    private int $count;
    private int $offset;
    private int $limit;

    public function __construct(PaginatorInterface $paginator)
    {
        $this->items = $paginator->toArray();
        $this->count = $paginator->count();
        $this->offset = $paginator->getOffset();
        $this->limit = $paginator->getLimit();
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return $this->count;
    }

    public function getItems(int $limit = null, int $offset = null): \Traversable
    {
        return $this->getIterator();
    }

    public function setOffset(int $offset): self
    {
        //noop
        return $this;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): self
    {
        //noop
        return $this;
    }
}
