<?php

declare(strict_types=1);

namespace kor3k\Pagination\Adapter;

interface AdapterInterface  extends \Countable, \IteratorAggregate
{
    public function getOffset(): int;
    public function setOffset(int $offset);
    public function getLimit(): int;
    public function setLimit(int $limit);
}
