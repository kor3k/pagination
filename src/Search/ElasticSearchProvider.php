<?php

declare(strict_types=1);

namespace kor3k\Pagination\Search;

use kor3k\Pagination\Adapter\ArrayAdapter;
use kor3k\Pagination\Adapter\ElasticaAdapter;
use kor3k\Pagination\Paginator\Paginator;
use kor3k\Pagination\Paginator\ExtractedPaginator;
use kor3k\Pagination\Paginator\PaginatorInterface;
use FOS\ElasticaBundle\Finder\PaginatedFinderInterface;

class ElasticSearchProvider implements SearchProviderInterface
{
    protected PaginatedFinderInterface $finder;

    public function __construct(PaginatedFinderInterface $finder)
    {
        $this->finder = $finder;
    }

    protected function prepareQuery(string $query): string
    {
        $query = strtr($query, [ "/" => "\\/" ]);
        $query = '*'.$query.'*';

        return $query;
    }

    public function search(string $query, int $offset, int $limit): PaginatorInterface
    {
        $query = $this->prepareQuery($query);

        try {
            $paginator = new Paginator(new ElasticaAdapter($this->finder->createPaginatorAdapter($query)));
            $paginator
                ->setOffset((int) $offset)
                ->setLimit((int) $limit);
            $paginator = new ExtractedPaginator($paginator);
        }
        catch (\RuntimeException $e) {
            $paginator = new Paginator(new ArrayAdapter([]));
        }

        return $paginator;
    }
}
