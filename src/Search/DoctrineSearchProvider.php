<?php

declare(strict_types=1);

namespace kor3k\Pagination\Search;

use kor3k\Pagination\Adapter\DoctrineAdapter;
use kor3k\Pagination\Paginator\Paginator;
use kor3k\Pagination\Paginator\PaginatorInterface;
use Doctrine\ORM\QueryBuilder;

class DoctrineSearchProvider implements SearchProviderInterface
{
    /**
     * @var \Closure|QueryBuilder
     */
    protected $queryBuilder;

    public function __construct($queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    protected function prepareQuery(string $query): string
    {
        $query = strtr($query, [ "/" => "\\/" ]);
        $query = '%'.$query.'%';

        return $query;
    }

    public function search(string $query, int $offset, int $limit): PaginatorInterface
    {
        $query = $this->prepareQuery($query);

        $queryBuilder = $this->queryBuilder;

        if($queryBuilder instanceof QueryBuilder) {
            $queryBuilder->setParameter(':query', $query);
            $qb = $queryBuilder;
        }
        else if($queryBuilder instanceof \Closure) {
            $qb = $queryBuilder($query);

            if(!($qb instanceof QueryBuilder)) {
                throw new \LogicException("must return QueryBuilder");
            }
        } else {
            throw new \LogicException('must be QueryBuilder|Closure');
        }

        $paginator = new Paginator(new DoctrineAdapter($qb));
        $paginator
            ->setOffset((int) $offset)
            ->setLimit((int) $limit);

        return $paginator;
    }
}
